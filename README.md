# EcoLogic.Auth0Api - Accessing Auth0 Management Api

**EcoLogic.Auth0Api** allows to access the auth0 management API.

## Implemented Methods

- Create a new user
- Get all users
- Get user by auth0-id
- Get users by email
- Update a users password
- Update a users email
- Update a users blocked status
- Check if users exists with a specific id
- Check if users exists with a specific email
- Delete a user

## Getting Started

### Startup.cs

The api client is added with the following configuration inside `ConfigureServices`

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddAuth0Api(options =>
    {
        options.Audience = "your-audience";
        options.BaseUrl = "https://your-tenant.auth0.com";
        options.ClientId = "your-client-id";
        options.ClientSecret = "your-client-secret";
        options.GrantType = "client_credentials";
    });
    services.AddControllers();
}
```

### Usage

Now the Api Services can be injected via dependency injection inside controllers / services:

```csharp
[ApiController]
[Route("[controller]")]
public class ExampleController : ControllerBase
{
    private readonly IAuth0UserApiService _auth0UserService;

    public ExampleController(IAuth0UserApiService auth0UserService)
    {
        _auth0UserService = auth0UserService;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        return Ok(await _auth0UserService.GetUsers());
    }
}
```
