using System.Net.Http;

namespace EcoLogic.Auth0Api.Shared
{
    public class Auth0ApiClient
    {
        public Auth0ApiClient(HttpClient client)
        {
            Client = client;
        }

        public HttpClient Client { get; }
    }
}