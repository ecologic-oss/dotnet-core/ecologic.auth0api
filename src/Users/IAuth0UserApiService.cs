using System.Collections.Generic;
using System.Threading.Tasks;
using EcoLogic.Auth0Api.Users.Models;

namespace EcoLogic.Auth0Api.Users
{
    public interface IAuth0UserApiService
    {
        Task<UserResponse> CreateUser(CreateUserRequest createUser);
        Task<IEnumerable<UserResponse>> GetUsers();
        Task<UserResponse> GetUserById(string id);
        Task<IEnumerable<UserResponse>> GetUsersByEmail(string email);
        Task<UserResponse> UpdateUserPassword(string id, string password);
        Task<UserResponse> UpdateUserEmail(string id, string email);
        Task<UserResponse> UpdateUserMetadata(string id, dynamic metadata);


        Task<bool> CheckIfUserExistsWithEmail(string email);
        Task<bool> CheckIfUserExistsWithId(string id);
        Task<UserResponse> DeleteUser(string id);
        Task<UserResponse> UpdateUserBlockStatus(string id, bool blocked);
    }
}