using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EcoLogic.Auth0Api.Authentication;
using EcoLogic.Auth0Api.Exceptions;
using EcoLogic.Auth0Api.Extensions;
using EcoLogic.Auth0Api.Shared;
using EcoLogic.Auth0Api.Users.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace EcoLogic.Auth0Api.Users
{
    public class Auth0UserApiService : IAuth0UserApiService
    {
        private readonly IAccessTokenResolver _accessTokenResolver;
        private readonly HttpClient _client;

        public Auth0UserApiService(Auth0ApiClient auth0ApiClient, IOptions<Auth0ApiOptions> options)
        {
            _accessTokenResolver = new AccessTokenResolver(auth0ApiClient, options);
            _client = auth0ApiClient.Client;
        }

        /// <summary>
        /// Creates a new Auth0 user
        /// </summary>
        public async Task<UserResponse> CreateUser(CreateUserRequest createUserRequest)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());

            if (await CheckIfUserExistsWithEmail(createUserRequest.Email))
            {
                throw new Auth0ManagementApiException(500, $"User with Email {createUserRequest.Email} is already existing.");
            }

            var response = await _client.PostAsJsonAsync("/api/v2/users", createUserRequest);
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }

        /// <summary>
        /// Returns alls Auth0 users.
        /// Warning: This is limited to 1000 users. In an application with >1000 users, this call will return a 400.
        /// See: https://auth0.com/docs/users/search/v3/view-search-results-by-page#limitation
        /// </summary>
        public async Task<IEnumerable<UserResponse>> GetUsers()
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var response = await _client.GetAsync("/api/v2/users");
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse[]>();
        }

        /// <summary>
        /// Gets a single Auth0 user by id
        /// </summary>
        /// <param name="id">auth0 id includign prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        public async Task<UserResponse> GetUserById(string id)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var response = await _client.GetAsync($"/api/v2/users/{id}");
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }

        /// <summary>
        /// Gets users by their email address
        /// </summary>
        /// <param name="email">auth0 user email. eg: info@mail.de</param>
        /// <returns></returns>
        public async Task<IEnumerable<UserResponse>> GetUsersByEmail(string email)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var response = await _client.GetAsync($"/api/v2/users-by-email?email={Uri.EscapeDataString(email.ToLower())}");
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<IEnumerable<UserResponse>>();
        }

        /// <summary>
        /// Updates a users password
        /// </summary>
        /// <param name="id">auth0 id includign prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        /// <param name="password">the new password</param>
        /// <returns></returns>
        public async Task<UserResponse> UpdateUserPassword(string id, string password)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var requestContent = new StringContent(JsonConvert.SerializeObject(new {password = password}));
            requestContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var response = await _client.PatchAsync($"/api/v2/users/{id}", requestContent);
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }

        /// <summary>
        /// Updates a users email
        /// </summary>
        /// <param name="id">auth0 id including prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        /// <param name="email">auth0 user email. eg: info@mail.de</param>
        public async Task<UserResponse> UpdateUserEmail(string id, string email)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var requestContent = new StringContent(JsonConvert.SerializeObject(new {email = email}));
            requestContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var response = await _client.PatchAsync($"/api/v2/users/{id}", requestContent);
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }

        /// <summary>
        /// Updates a users metadata
        /// The data is merged into the existing data
        /// </summary>
        /// <param name="id">auth0 id including prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        /// <param name="metadata">The metadata to be merged in eg: { firstname: 'new name' }</param>
        public async Task<UserResponse> UpdateUserMetadata(string id, dynamic metadata)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var requestContent = new StringContent(JsonConvert.SerializeObject(new {user_metadata = metadata}));
            requestContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var response = await _client.PatchAsync($"/api/v2/users/{id}", requestContent);
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }

        /// <summary>
        /// Checks if a user with a specific email exists
        /// </summary>
        /// <param name="email">auth0 user email. eg: info@mail.de</param>
        public async Task<bool> CheckIfUserExistsWithEmail(string email)
        {
            return (await GetUsersByEmail(email)).Any();
        }

        /// <summary>
        /// Checks if a user with a specific id exists
        /// </summary>
        /// <param name="id">auth0 id includign prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        public async Task<bool> CheckIfUserExistsWithId(string id)
        {
            try
            {
                await GetUserById(id);
                return true;
            }
            catch (Auth0ManagementApiException ex) when (ex.StatusCode == 404)
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes a user with a specific auth0-id
        /// </summary>
        /// <param name="id">auth0 id includign prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        public async Task<UserResponse> DeleteUser(string id)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var response = await _client.DeleteAsync($"/api/v2/users/{id}");
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }

        /// <summary>
        /// Blocks a user with a specific auth0-id
        /// </summary>
        /// <param name="id">auth0 id includign prefix. eg: auth0|5c19a8c0beaac216496f825a</param>
        /// <param name="blocked">the new status (true for blocked, false for not-blocked)</param>
        public async Task<UserResponse> UpdateUserBlockStatus(string id, bool blocked)
        {
            _client.SetTokenHeader(await _accessTokenResolver.GetAccessToken());
            var requestContent = new StringContent(JsonConvert.SerializeObject(new {blocked = blocked}));
            requestContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            var response = await _client.PatchAsync($"/api/v2/users/{id}", requestContent);
            await response.ThrowExceptionWithDetailsIfUnsuccessful();
            return await response.Content.ReadAsAsync<UserResponse>();
        }
    }
}