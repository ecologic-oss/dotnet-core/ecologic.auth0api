using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EcoLogic.Auth0Api.Users.Models
{
    public class UserResponse
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "email_verified")]
        public string EmailVerified { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "last_password_reset")]
        public DateTime LastPasswordReset { get; set; }

        [JsonProperty(PropertyName = "last_login")]
        public DateTime LastLogin { get; set; }

        [JsonProperty(PropertyName = "picture")]
        public string Picture { get; set; }

        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }

        [JsonProperty(PropertyName = "logins_count")]
        public int LoginsCount { get; set; }

        [JsonProperty(PropertyName = "blocked")]
        public bool Blocked { get; set; }

        [JsonProperty(PropertyName = "user_metadata")]
        public JObject UserMetadata { get; set; }
    }
}