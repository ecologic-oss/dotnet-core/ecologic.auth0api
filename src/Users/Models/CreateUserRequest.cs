using Newtonsoft.Json;

namespace EcoLogic.Auth0Api.Users.Models
{
    public class CreateUserRequest
    {
        /// <summary>
        /// ID of the connection this user should be created in.
        /// </summary>
        [JsonProperty(PropertyName = "connection")]
        public string Connection { get; set; }

        /// <summary>
        /// The user's email.
        /// </summary>
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        /// <summary>
        /// Whether this email address is verified (true) or unverified (false).
        /// </summary>
        [JsonProperty(PropertyName = "email_verified")]
        public bool EmailVerified { get; set; }

        /// <summary>
        /// Initial password for this user (mandatory for non-SMS connections).
        /// </summary>
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}