using System;

namespace EcoLogic.Auth0Api.Exceptions
{
    public class Auth0ManagementApiException : Exception
    {
        public Auth0ManagementApiException(int statusCode, string message) : base($"[{statusCode}]: {message})")
        {
            StatusCode = statusCode;
        }

        public int StatusCode { get; }
    }
}