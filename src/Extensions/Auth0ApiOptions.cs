namespace EcoLogic.Auth0Api.Extensions
{
    public class Auth0ApiOptions
    {
        public string BaseUrl { get; set; }
        public string Audience { get; set; }
        public string ClientId { get; set; }
        public string GrantType { get; set; }
        public string ClientSecret { get; set; }
    }
}