using System;
using EcoLogic.Auth0Api.Shared;
using EcoLogic.Auth0Api.Users;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace EcoLogic.Auth0Api.Extensions
{
    public static class Auth0ApiExtensions
    {
        public static IServiceCollection AddAuth0Api(this IServiceCollection services, Action<Auth0ApiOptions> options)
        {
            services.Configure(options);
            var resolvedOptions = (IOptions<Auth0ApiOptions>) services.BuildServiceProvider().GetService(typeof(IOptions<Auth0ApiOptions>));
            services.AddHttpClient<Auth0ApiClient>(client => { client.BaseAddress = new Uri(resolvedOptions.Value.BaseUrl); });
            services.AddTransient<IAuth0UserApiService, Auth0UserApiService>();
            return services;
        }
    }
}