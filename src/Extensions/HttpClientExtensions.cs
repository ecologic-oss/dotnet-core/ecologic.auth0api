using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EcoLogic.Auth0Api.Authentication.Models;

namespace EcoLogic.Auth0Api.Extensions
{
    internal static class HttpClientExtensions
    {
        internal static void SetTokenHeader(this HttpClient client, TokenResponse token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.TokenType, token.AccessToken);
        }

        internal static async Task<HttpResponseMessage> PatchAsync(this HttpClient client, string uri, HttpContent content)
        {
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, new Uri(CombineBaseUrlWithSegment(client.BaseAddress.ToString(), uri))) {Content = content};
            return await client.SendAsync(request);
        }

        private static string CombineBaseUrlWithSegment(string uri1, string uri2)
        {
            return $"{uri1.TrimEnd('/')}/{uri2.TrimStart('/')}";
        }
    }
}