using System.Net.Http;
using System.Threading.Tasks;
using EcoLogic.Auth0Api.Exceptions;

namespace EcoLogic.Auth0Api.Extensions
{
    internal static class HttpResponseMessageExtensions
    {
        internal static async Task ThrowExceptionWithDetailsIfUnsuccessful(this HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                throw new Auth0ManagementApiException((int) response.StatusCode, await response.Content.ReadAsStringAsync());
            }
        }
    }
}