using Newtonsoft.Json;

namespace EcoLogic.Auth0Api.Authentication.Models
{
    internal class TokenResponse
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }
    }
}