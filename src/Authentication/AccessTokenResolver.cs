using System.Net.Http;
using System.Threading.Tasks;
using EcoLogic.Auth0Api.Authentication.Models;
using EcoLogic.Auth0Api.Extensions;
using EcoLogic.Auth0Api.Shared;
using Microsoft.Extensions.Options;

namespace EcoLogic.Auth0Api.Authentication
{
    internal class AccessTokenResolver : IAccessTokenResolver
    {
        private readonly IOptions<Auth0ApiOptions> _options;
        private readonly HttpClient _client;

        internal AccessTokenResolver(Auth0ApiClient auth0ApiClient, IOptions<Auth0ApiOptions> options)
        {
            _client = auth0ApiClient.Client;
            _options = options;
        }

        public async Task<TokenResponse> GetAccessToken()
        {
            var tokenRequestBody = new TokenRequest
            {
                Audience = _options.Value.Audience,
                ClientId = _options.Value.ClientId,
                GrantType = _options.Value.GrantType,
                ClientSecret = _options.Value.ClientSecret
            };
            var response = await _client.PostAsJsonAsync("/oauth/token", tokenRequestBody);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<TokenResponse>();
        }
    }
}