using System.Threading.Tasks;
using EcoLogic.Auth0Api.Authentication.Models;

namespace EcoLogic.Auth0Api.Authentication
{
    internal interface IAccessTokenResolver
    {
        Task<TokenResponse> GetAccessToken();
    }
}